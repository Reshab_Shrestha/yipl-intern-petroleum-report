from rest_framework import serializers

from Product.models import Report


class ReportModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Report
        fields = ['Name', 'Year', 'Sale']

    def create(self, validated_data):
        print(self.data)
