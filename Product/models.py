from django.db import models


# Create your models here.
class Product(models.Model):
    Name = models.CharField(max_length=100)

    def __str__(self):
        return str(self.Name)


class Report(models.Model):
    Year = models.CharField(max_length=50)
    Sale = models.IntegerField()
    Name = models.ForeignKey(Product, on_delete=models.CASCADE)

    def __str__(self):
        return '{} {} {}'.format(self.Name, self.Year, self.Sale)
