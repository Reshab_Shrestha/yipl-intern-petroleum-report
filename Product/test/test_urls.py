from django.test import SimpleTestCase
from django.urls import reverse, resolve

from Product.views import product, generate_report


class TestUrls(SimpleTestCase):
    def test_product_url_is_resolved(self):
        url = reverse('product')
        self.assertEquals(resolve(url).func, product)

    def test_report_url_is_resolved(self):
        url = reverse('report')
        self.assertEquals(resolve(url).func, generate_report)
