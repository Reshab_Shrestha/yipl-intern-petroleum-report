from django.test import TestCase

from Product.models import Product, Report


class TestModels(TestCase):
    def setUp(self):
        self.proj = Product.objects.create(Name='Hello')

    def test_Report_created(self):
        self.proj1 = Report.objects.create(
            Name=self.proj,
            Year='2020',
            Sale='1234'
        )
        self.assertEquals(str(self.proj1), "Hello 2020 1234")

    def test_Product_created(self):
        self.proj2 = Product.objects.create(Name='Hello')
        self.assertEquals(str(self.proj2), "Hello")
