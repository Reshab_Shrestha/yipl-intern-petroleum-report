from django.test import TestCase, Client
from django.urls import reverse, resolve


class TestViews(TestCase):
    def test_product(self):
        client = Client()
        response = client.get(reverse('report'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'Report.html')
