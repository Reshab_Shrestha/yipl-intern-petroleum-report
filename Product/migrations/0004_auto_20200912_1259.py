# Generated by Django 3.1 on 2020-09-12 07:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Product', '0003_auto_20200911_1913'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='Year',
            field=models.CharField(max_length=50),
        ),
    ]
