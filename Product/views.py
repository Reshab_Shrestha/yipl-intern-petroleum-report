# Create your views here.
import requests
from django.db.models import Min, Max, Avg
from django.db.models.functions import Coalesce
from django.http import HttpResponse
from django.shortcuts import render

from Product.models import Product, Report


def product(request):
    response = requests.get(
        'https://raw.githubusercontent.com/younginnovations/internship-challenges/master/programming/petroleum'
        '-report/data.json')
    data = response.json()
    for i in data:
        year = i['year']
        name = i['petroleum_product']
        sale = i['sale']
        try:
            obj = Product.objects.get(Name=name)
        except Product.DoesNotExist:
            obj = Product.objects.create(Name=name)
            obj.save()
        report = Report.objects.create(Year=year, Sale=sale, Name=obj)
        report.save()
    return HttpResponse("Product Created")


def generate_report(request):
    rej = []
    for i in Product.objects.all():
        a = Report.objects.filter(Name=i)
        b = a.filter(Year__gte=2010, Year__lte=2014)
        c = a.filter(Year__gte=2005, Year__lte=2009)
        d = a.filter(Year__gte=2000, Year__lte=2004)
        r1 = {
            'Name': i,
            'Year': b[4].Year + "-" + b[0].Year,
            'Result': b.exclude(Sale=0).aggregate(Max=Coalesce(Max('Sale'), 0), Min=Coalesce(Min('Sale'), 0),
                                                  Avg=Coalesce(Avg('Sale'), 0))
        }
        r2 = {
            'Name': i,
            'Year': c[4].Year + "-" + c[0].Year,
            'Result': c.exclude(Sale=0).aggregate(Max=Coalesce(Max('Sale'), 0), Min=Coalesce(Min('Sale'), 0),
                                                  Avg=Coalesce(Avg('Sale'), 0))
        }
        r3 = {
            'Name': i,
            'Year': d[4].Year + "-" + d[0].Year,
            'Result': d.exclude(Sale=0).aggregate(Max=Coalesce(Max('Sale'), 0), Min=Coalesce(Min('Sale'), 0),
                                                  Avg=Coalesce(Avg('Sale'), 0))
        }

        rej.append(r1)
        rej.append(r2)
        rej.append(r3)

    return render(request, 'Report.html', {'context': rej})
