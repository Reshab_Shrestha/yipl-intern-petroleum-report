from django.contrib import admin

# Register your models here.
from Product.models import Product, Report

admin.site.register(Product)
admin.site.register(Report)
