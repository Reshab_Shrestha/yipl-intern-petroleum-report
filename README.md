```bash
pip install -r requirements.txt

python manage.py migrate

python manage.py runserver
```

#Navigate
http://127.0.0.1:8000/product/ 
to fetch data from API and store into models

http://127.0.0.1:8000/report/ 
to generate report and present in html

#Tests
To run the test, cd into the directory where manage.py is 
```sh
(env)$ python manage.py test 
```